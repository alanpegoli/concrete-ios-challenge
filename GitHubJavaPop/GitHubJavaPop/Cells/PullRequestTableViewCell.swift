//
//  PullRequestTableViewCell.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit
import Kingfisher

class PullRequestTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userFullNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp(pullRequest: PullRequest) {
        
        self.titleLabel.text = pullRequest.title
        self.descriptionLabel.text = pullRequest.description
        self.usernameLabel.text = pullRequest.author.name
        self.userFullNameLabel.text = pullRequest.author.name
        
        if let imageURL = URL(string: pullRequest.author.profilePicSource) {
            
            let imageResource = ImageResource(downloadURL: imageURL)
            self.userImageView.kf.setImage(with: imageResource)
            
            // I thought it would be better a rounded-square rather than a circle for the avatars
            self.userImageView.layer.cornerRadius = 4
            self.userImageView.layer.masksToBounds = true
        }
    }
}
