//
//  RepositoryTableViewCell.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoryTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var forkIconImageView: UIImageView!
    @IBOutlet weak var starIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp(repository: Repository) {
        
        self.nameLabel.text = repository.name
        self.descriptionLabel.text = repository.description
        self.usernameLabel.text = repository.author.name
        self.userFullNameLabel.text = repository.author.name
        
        self.forksLabel.text = "\(repository.forksCount)"
        self.starsLabel.text = "\(repository.stargazersCount)"
        
        if let imageURL = URL(string: repository.author.profilePicSource) {
            
            let imageResource = ImageResource(downloadURL: imageURL)
            self.userImageView.kf.setImage(with: imageResource)
            
            // I thought it would be better a rounded-square rather than a circle for the avatars
            self.userImageView.layer.cornerRadius = 4
            self.userImageView.layer.masksToBounds = true
        }
        
        self.forkIconImageView.image = UIImage(named: "fork")?.withRenderingMode(.alwaysTemplate)
        self.forkIconImageView.tintColor = UIColor.darkOrange
        
        self.starIconImageView.image = UIImage(named: "star")?.withRenderingMode(.alwaysTemplate)
        self.starIconImageView.tintColor = UIColor.darkOrange
    }
}
