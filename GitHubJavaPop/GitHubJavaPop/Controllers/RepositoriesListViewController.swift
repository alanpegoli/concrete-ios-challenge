//
//  RepositoriesListViewController.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit

class RepositoriesListViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var repositoriesTableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Properties
    var repositories: [Repository] = []
    var page: Int = 1
    var dataSource: RepositoriesDataSource?
    
    // MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUp()
        self.loadData(fromPage: self.page)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    func setUp() {
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        setUpTableView()
    }
    
    func setUpTableView() {
        
        self.dataSource = RepositoriesDataSource(repositories: self.repositories, tableView: self.repositoriesTableView)
        
        self.repositoriesTableView.delegate = self
        self.repositoriesTableView.dataSource = self.dataSource
    }
    
    
    
    
    func loadData(fromPage page: Int) {
        
        self.spinner.startAnimating()
        
        AlanHub.requestRepositories(page: page) { (result) in
            
            self.spinner.stopAnimating()
            
            switch result {
    
            case .success(let repo):
                
                self.repositories += repo.items
                
                if self.repositories.isEmpty {
                    
                    self.presentAlertError(withMessage: Glossary.Error.noRepository.rawValue)
                    
                } else {

                    self.dataSource?.repositories = self.repositories
                    self.repositoriesTableView.reloadData()

                    self.page += 1

                }
                break
                
            case .error(let error):
                
                self.presentAlertError(withMessage: error.localizedDescription)
                
            }
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Glossary.Segue.toPullsRequests.rawValue {
            
            if let pullsListViewController = segue.destination as? PullsListViewController {
                if let sender = sender as? Repository {
                    pullsListViewController.repo = sender
                }
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension RepositoriesListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: Glossary.Segue.toPullsRequests.rawValue, sender: self.repositories[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.repositories.count-5 {
            loadData(fromPage: self.page)
        }
    }
}
