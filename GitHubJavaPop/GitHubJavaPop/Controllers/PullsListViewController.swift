//
//  PullsListViewController.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit

class PullsListViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var pullsTableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Properties
    var repo: Repository!
    var pulls: [PullRequest] = []
    var page: Int = 1
    var dataSource: PullRequestsDataSource = PullRequestsDataSource()
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUp()
        self.loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func setUp() {
    
        self.title = repo.name
        
        self.dataSource = PullRequestsDataSource(pulls: self.pulls, tableView: self.pullsTableView)
        
        self.pullsTableView.dataSource = self.dataSource
        self.pullsTableView.delegate = self
    }
    
    func loadData() {
        
        self.spinner.startAnimating()
        
        AlanHub.requestPullsRequest(repo: self.repo) { (result) in
            
            self.spinner.stopAnimating()
            
            switch result {
            
            case .success(let pulls):
                
                self.pulls += pulls
                
                if self.pulls.isEmpty {
                    
                    self.presentAlertError(withMessage: Glossary.Error.noPullRequests.rawValue)
                    
                } else {
                    
                    self.dataSource.pulls = self.pulls
                    self.pullsTableView.reloadData()
                }
                
                break
                
            case .error(let error):
                
                self.presentAlertError(withMessage: error.localizedDescription)
                
                break
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension PullsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let url = URL(string: self.pulls[indexPath.row].url) {
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
