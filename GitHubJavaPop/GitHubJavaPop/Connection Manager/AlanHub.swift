//
//  AlanHub.swift
//  GitHubJavaPop
//
//  Created by Vinicius França Nunes Silva on 27/12/2017.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation

enum AHResult<T> {
    case success(T)
    case error(AOFError)
}

class AlanHub {
    
    static func requestRepositories(page: Int, _ completion: @escaping (AHResult<RepositoriesJSONData>) -> Void) {
        
        AlanOnFire.requestJSON(from: "https://api.github.com/search/repositories", params: ["q": "language:Java", "sort": "stars", "page": page]) { result in
            switch result {
            case .success(let data):
                do {
                    let repositoriesJSONData = try JSONDecoder().decode(RepositoriesJSONData.self, from: data)
                    completion(.success(repositoriesJSONData))
                } catch {
                    completion(.error(AOFError.decoderJson))
                }
                break
            case .error(let error):
                completion(.error(error))
                break
            }
        }
        
    }
    
    static func requestPullsRequest(repo: Repository, _ completion: @escaping (AHResult<[PullRequest]>) -> Void) {
        
        AlanOnFire.requestJSON(from: "https://api.github.com/repos/\(repo.fullName)/pulls") { result in
            switch result {
            case .success(let data):
                do {
                    let pullRequest = try JSONDecoder().decode([PullRequest].self, from: data)
                    completion(.success(pullRequest))
                } catch {
                    completion(.error(AOFError.decoderJson))
                }
                break
            case .error(let error):
                completion(.error(error))
                break
            }
        }
        
    }
    
}
