//
//  AlanOnFire.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation

enum AOFError: Error {
    case noData
    case invalidURL
    case decoderJson
}

enum AOFResult {
    case success(Data)
    case error(AOFError)
}

class AlanOnFire {
    
    static func requestJSON(from urlString: String, params: [String: Any] = [:], completionHandler: @escaping (_ result: AOFResult) -> Void) {
        
        guard var urlComponent = URLComponents(string: urlString) else {
            completionHandler(AOFResult.error(AOFError.invalidURL))
            return
        }
        
        var queryItems : [URLQueryItem] = []
        for param in params {
            queryItems.append(URLQueryItem(name: param.key, value: "\(param.value)"))
        }
        
        urlComponent.queryItems = queryItems
        
        if let url = urlComponent.url {
            
            let session = URLSession(configuration: .default)
            
            session.dataTask(with: url, completionHandler: { (data, _, error) in
                
                if error == nil {
                    
                    if let dataTemp = data {
                        
                        DispatchQueue.main.async {
                            completionHandler(AOFResult.success(dataTemp))
                        }
                        
                    } else {
                        
                        completionHandler(AOFResult.error(AOFError.noData))
                    }
                }
            }).resume()
            
        } else {
            completionHandler(AOFResult.error(AOFError.invalidURL))
        }
       
    }
}

