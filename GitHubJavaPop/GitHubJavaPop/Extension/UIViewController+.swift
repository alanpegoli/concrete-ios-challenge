//
//  UIViewController+.swift
//  GitHubJavaPop
//
//  Created by Vinicius França Nunes Silva on 27/12/2017.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentAlertError(withMessage message: String) {
        
        let alert: UIAlertController = UIAlertController(title: Glossary.Alert.title.rawValue, message: message, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: Glossary.Alert.button.rawValue, style: .default, handler: { (alert) in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
