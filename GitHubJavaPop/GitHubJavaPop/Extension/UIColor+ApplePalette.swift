import UIKit

extension UIColor {

	/**
	 name: Red
	 red: 1.0000000000
	 green: 0.0000000000
	 blue: 0.0000000000
	 alpha: 1.0000000000
	 hex: #FF0000
	 **/

	static var red: UIColor {
		return UIColor(red: 1.0000000000, green: 0.0000000000, blue: 0.0000000000, alpha: 1.0000000000);
	}

	/**
	 name: Dark Orange
	 red: 1.0000000000
	 green: 0.5500000119
	 blue: 0.0000000000
	 alpha: 1.0000000000
	 hex: #FF8C00
	 **/

	static var darkOrange: UIColor {
		return UIColor(red: 1.0000000000, green: 0.5500000119, blue: 0.0000000000, alpha: 1.0000000000);
	}

	/**
	 name: Tangerine
	 red: 1.0000000000
	 green: 0.8100000024
	 blue: 0.0000000000
	 alpha: 1.0000000000
	 hex: #FFCF00
	 **/

	static var tangerine: UIColor {
		return UIColor(red: 1.0000000000, green: 0.8100000024, blue: 0.0000000000, alpha: 1.0000000000);
	}

	/**
	 name: Malachite
	 red: 0.0000000000
	 green: 0.8999999762
	 blue: 0.4000000060
	 alpha: 1.0000000000
	 hex: #00E666
	 **/

	static var malachite: UIColor {
		return UIColor(red: 0.0000000000, green: 0.8999999762, blue: 0.4000000060, alpha: 1.0000000000);
	}

	/**
	 name: Spiro Disco Ball
	 red: 0.1899999976
	 green: 0.7900000215
	 blue: 0.9900000095
	 alpha: 1.0000000000
	 hex: #30C9FC
	 **/

	static var spiroDiscoBall: UIColor {
		return UIColor(red: 0.1899999976, green: 0.7900000215, blue: 0.9900000095, alpha: 1.0000000000);
	}

	/**
	 name: Blue
	 red: 0.0700000003
	 green: 0.4199999869
	 blue: 1.0000000000
	 alpha: 1.0000000000
	 hex: #126BFF
	 **/

	static var blue: UIColor {
		return UIColor(red: 0.0700000003, green: 0.4199999869, blue: 1.0000000000, alpha: 1.0000000000);
	}

	/**
	 name: Majorelle Blue
	 red: 0.4000000060
	 green: 0.2399999946
	 blue: 0.8500000238
	 alpha: 1.0000000000
	 hex: #663DD9
	 **/

	static var majorelleBlue: UIColor {
		return UIColor(red: 0.4000000060, green: 0.2399999946, blue: 0.8500000238, alpha: 1.0000000000);
	}

	/**
	 name: Carmine
	 red: 1.0000000000
	 green: 0.0000000000
	 blue: 0.2700000107
	 alpha: 1.0000000000
	 hex: #FF0045
	 **/

	static var carmine: UIColor {
		return UIColor(red: 1.0000000000, green: 0.0000000000, blue: 0.2700000107, alpha: 1.0000000000);
	}

}
