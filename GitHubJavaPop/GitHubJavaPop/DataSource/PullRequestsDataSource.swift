//
//  PullRequestsDataSource.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 04/11/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit

class PullRequestsDataSource: NSObject, UITableViewDataSource {
    
    var pulls: [PullRequest] = []
    weak var tableView: UITableView?
    
    let reuseIdentifier: String = "PullRequestTableViewCell"
    
    override init() {
        super.init()
        
        self.pulls = []
        self.tableView = nil
    }
    
    init(pulls: [PullRequest], tableView: UITableView) {
        super.init()
        
        self.pulls = pulls
        self.tableView = tableView
        
        self.tableView?.register(UINib(nibName: "PullRequestTableViewCell", bundle: nil), forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pulls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath)
        guard let pullRequestTableViewCell = cell as? PullRequestTableViewCell else { return cell }
        
        pullRequestTableViewCell.setUp(pullRequest: self.pulls[indexPath.row])
        
        return pullRequestTableViewCell
    }
}
