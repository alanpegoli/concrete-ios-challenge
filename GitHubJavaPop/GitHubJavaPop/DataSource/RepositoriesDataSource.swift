//
//  RepositoriesDataSource.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 30/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import UIKit

class RepositoriesDataSource: NSObject, UITableViewDataSource {
    
    var repositories: [Repository] = []
    let reuseIdentifier: String = "RepositoryTableViewCell"
    
    init(repositories: [Repository], tableView: UITableView) {
        super.init()
        self.repositories = repositories
        self.registerCells(tableView: tableView)
    }
    
    private func registerCells(tableView: UITableView){
        let nib = UINib(nibName: self.reuseIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath)
        guard let repositoryTableViewCell = cell as? RepositoryTableViewCell else { return cell }
        
        repositoryTableViewCell.setUp(repository: self.repositories[indexPath.row])
        
        return repositoryTableViewCell
    }
}
