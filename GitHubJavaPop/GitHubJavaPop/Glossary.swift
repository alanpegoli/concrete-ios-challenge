//
//  String+.swift
//  GitHubJavaPop
//
//  Created by Vinicius França Nunes Silva on 27/12/2017.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation


enum Glossary {
    
    enum Error: String {
        case noRepository = "There is no repository to fetch."
        case noPullRequests = "There is no pull requests to fetch."
    }
    
    enum Alert: String {
        case title = "Error 😿"
        case button = "OK"
    }
    
    enum Segue: String {
        case toPullsRequests = "segueToPullsRequests"
    }
    
}

