//
//  Author.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation

class Author: Codable {

    var name: String
    var profilePicSource: String
    
    private enum CodingKeys: String, CodingKey {
        case name = "login"
        case profilePicSource = "avatar_url"
    }
}
