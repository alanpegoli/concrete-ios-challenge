//
//  Repository.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 29/10/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation

class Repository: Codable {

    var name: String
    var fullName: String
    var description: String
    var author: Author
    var stargazersCount: Int
    var forksCount: Int
    
    private enum CodingKeys: String, CodingKey {
        case name
        case fullName = "full_name"
        case description
        case author = "owner"
        case stargazersCount = "stargazers_count"
        case forksCount = "forks_count"
    }
    
}
