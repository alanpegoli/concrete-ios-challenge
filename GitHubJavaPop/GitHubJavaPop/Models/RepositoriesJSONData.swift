//
//  RepositoriesJSONData.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 02/11/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation

class RepositoriesJSONData: Codable {
    
    var items: [Repository]
    
    private enum CodingKeys: String, CodingKey {
        case items
    }
}
