//
//  PullRequest.swift
//  GitHubJavaPop
//
//  Created by Alan Henrique Pégoli on 04/11/17.
//  Copyright © 2017 Alan Henrique Pégoli. All rights reserved.
//

import Foundation

class PullRequest: Codable {
    
    var title: String
    var description: String?
    var author: Author
    var url: String
    
    private enum CodingKeys: String, CodingKey {
        case title
        case description = "body"
        case author = "user"
        case url = "html_url"
    }
}
